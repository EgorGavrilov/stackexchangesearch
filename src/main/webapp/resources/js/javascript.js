var pageNumber = 1;
$(document).ready(function () {
    hideElements();
});

var query = "";

$(document).keypress(function(e){
    if (e.which == 13){
        $('#searchButton').click();
    }
});

function searchFunction() {
    query = encodeURI(document.getElementById("inputQuery").value);
    $("#result-div  > tbody").empty();
    if (query === "") {
        hideElements();
        return;
    }
    pageNumber = 1;
    $.ajax({
        type: "get",
        async: false,
        url: "/search",
        data: {
            searchString: query
        },
        success: function (response) {
            appendResponse(response)
        },
        error: function (xhr) {
            showFail(xhr)
        }
    })
}

function appendResponse(response) {
    $("#results").show();
    $.each(response.items, function (i, item) {

        var answer_count = "<td>" + item.answer_count + "</td>";
        if (item.is_answered) {
            answer_count = "<td class='answered'>" + item.answer_count + "</td>";
        }

        var d = new Date(item.creation_date);
        var user = "<td><a href='" + item.owner.link + "'>" + item.owner.display_name + "</a></td>";
        var date = "<td>" + d.toLocaleString() + "</td>";
        var link = "<td><a href='" + item.link + "'>" + item.title + "</a></td>";
        var result = "<tr>" + answer_count + user + link + date +"</tr>";
        $("#result-div  > tbody:last-child").append(result)
    });

    if (response.has_more) {
        $(".appendNextButton").show();
    } else {
        $(".appendNextButton").hide();
    }
}

function appendNextResultFunction(){
    pageNumber = pageNumber+1;
    $.ajax({
        type: "get",
        url: "/search",
        data: {
            searchString: query,
            page: pageNumber
        },
        success: function (response) {
            appendResponse(response)
        },
        error: function (xhr) {
            showFail(xhr)
        }
    })
}

function hideElements() {
    $(".appendNextButton").hide();
    $("#results").hide()
}

function showFail(xhr) {
    alert(xhr.responseText)
}