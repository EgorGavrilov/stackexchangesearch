package egavrilov.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {
    @JsonProperty("display_name")
    public String name;

    @JsonProperty("link")
    public String link;
}
