package egavrilov.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Response {

    @JsonProperty("items")
    public List<QuestionDto> items;

    @JsonProperty("has_more")
    public Boolean hasMore;

}