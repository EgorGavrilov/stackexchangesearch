package egavrilov.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.List;

public class QuestionDto {

    @JsonProperty("tags")
    public List<String> tags;

    @JsonProperty("is_answered")
    public boolean isAnswered;

    @JsonProperty("link")
    public String link;

    @JsonProperty("title")
    public String title;

    @JsonProperty("owner")
    public UserDto user;

    @JsonDeserialize(using = DateDeserializer.class)
    @JsonSerialize(using = DateSerializer.class)
    @JsonProperty("creation_date")
    public Date questionDate;

    @JsonProperty("answer_count")
    public Integer answerCount;
}