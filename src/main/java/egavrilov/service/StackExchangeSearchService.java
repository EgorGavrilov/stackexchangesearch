package egavrilov.service;

import egavrilov.common.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StackExchangeSearchService implements SearchService {

    @Autowired
    private ServiceClient serviceClient;

    @Override
    public Response search(String query, Pageable pageRequest) {
        return serviceClient.search(query, pageRequest);
    }
}
