package egavrilov.service;

import egavrilov.common.Response;
import org.springframework.data.domain.Pageable;

public interface SearchService {
    Response search(String query, Pageable pageRequest);
}
