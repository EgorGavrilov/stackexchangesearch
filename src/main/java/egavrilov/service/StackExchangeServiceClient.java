package egavrilov.service;

import egavrilov.common.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class StackExchangeServiceClient implements ServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${stack.exchange.url}")
    private String stackExchangeUrl;
    @Value("${site.for.search}")
    private String site;

    @Override
    public Response search(String searchString, Pageable pageRequest) {

        URI searchUri = UriComponentsBuilder.fromHttpUrl(stackExchangeUrl)
                .path("/search")
                .queryParam("intitle", searchString.replace("%20", "+"))
                .queryParam("page", pageRequest.getPageNumber())
                .queryParam("pagesize", pageRequest.getPageSize())
                .queryParam("site", site)
                .build()
                .toUri();


        ResponseEntity<Response> responseEntity = restTemplate.exchange(
                searchUri, HttpMethod.GET, null, Response.class
        );

        return responseEntity.getBody();
    }
}
