package egavrilov.service;

import egavrilov.common.Response;
import org.springframework.data.domain.Pageable;

public interface ServiceClient {
    Response search(String searchString, Pageable pageRequest);
}
