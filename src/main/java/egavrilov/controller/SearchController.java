package egavrilov.controller;

import egavrilov.common.Response;
import egavrilov.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {

    @Autowired
    private SearchService searchService;


    @GetMapping("/search")
    public ResponseEntity<Response> searchInStackoverflow(@RequestParam("searchString") String query,
                                                          @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                          @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        Response response = searchService.search(query, PageRequest.of(page, pageSize));
        return ResponseEntity.ok(response);
    }
}
